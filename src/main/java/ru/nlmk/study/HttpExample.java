package ru.nlmk.study;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.List;

public class HttpExample {

    private static final String GET_CATEGORIES = "https://api.chucknorris.io/jokes/categories";

    private static final String GET_JOKE = "https://api.chucknorris.io/jokes/random?category=";

    public String printCategories() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(GET_CATEGORIES))
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }

    public List<String> getUri(String body){
        body = body.replace("[", "");
        body = body.replace("\"","");
        String[] result = body.split(",");
        return Arrays.asList(result);
    }

    public void printRandomJoke() throws IOException, InterruptedException {
        String body = printCategories();
        List<String> categories = getUri(body);
        System.out.println(categories);

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request;
        for(String s : categories){
            request = java.net.http.HttpRequest.newBuilder()
                    .uri(URI.create(GET_JOKE.concat(s)))
                    .build();
            client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(HttpResponse::body)
                    .thenAccept(System.out::println)
                    .join();
        }
    }

}
