package ru.nlmk.study;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        HttpExample httpExample = new HttpExample();
        try {
            httpExample.printRandomJoke();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }

}
